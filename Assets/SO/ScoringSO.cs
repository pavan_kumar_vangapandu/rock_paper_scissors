using System.Collections;
using System.Collections.Generic;
using UnityEngine;
[CreateAssetMenu(fileName = "Data", menuName = "ScriptableObjects/Data", order = 1)]
public class ScoringSO : ScriptableObject
{
    public float timer;
    public int scoringPoints;
}
