using UnityEngine;
using System.Collections;
using UnityEditor;
using System.IO;

namespace NT
{
	public class Delete_All_PlayerPrefs : Editor
	{
		[MenuItem ("Delete Prefs/Delete All PlayerPrefs #d")]

		static void Init ()
		{
			PlayerPrefs.DeleteAll ();
			Caching.ClearCache ();
			

			Debug.Log ("All PlayerPrefs Deleted..");
			EditorUtility.DisplayDialog ("Deleted", "All PlayerPrefs Deleted...!", "OK");
		}
	}
}

