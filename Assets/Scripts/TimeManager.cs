using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;

public class TimeManager : MonoBehaviour {
    public ScoringSO data;

    public float timer;

    Slider timerslider;
    GamePlayController controller;
    private void Awake () {
        timerslider = GetComponent<Slider> ();
        controller = GameObject.FindObjectOfType<GamePlayController> ();
    }
    private void OnEnable () {
        ResetTimer ();
    }

    // Update is called once per frame
    void Update () {
        if (controller.gameStates == GameStates.GamePlay && controller.playerChoice == GameChoices.NONE) {
            
            
                if (timer <= 0) {

                    controller.gameStates = GameStates.MainMenu;
                    controller.updategameState.Invoke ();

                    return;
                }
                timer -= Time.deltaTime;
                timerslider.value = timer;
            

        }

    }
    public void ResetTimer () {
        timer = data.timer;
        timerslider.maxValue = timer;
    }
}