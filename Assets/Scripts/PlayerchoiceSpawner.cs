using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using System;
using UnityEngine.UI;

public class PlayerchoiceSpawner : MonoBehaviour
{
    public GameObject choicePrefab;
    public Transform choiceParent;
    // Start is called before the first frame update
   private void OnEnable() {
       DestoryAllChildren();
       SpawnChoices();
   }
   public void SpawnChoices()
   {
       int count = Enum.GetNames(typeof(GameChoices)).Length;
       for(int i = 1;i< count;i++)
       {
           GameObject choice = Instantiate(choicePrefab,choiceParent);
           choice.GetComponentInChildren<Text>().text = ((GameChoices)i).ToString().ToUpper();
           choice.GetComponent<RectTransform>().sizeDelta = new Vector2(100,100);
           choice.AddComponent<PlayerInputManager>();
           PlayerInputManager inputManager = choice.GetComponent<PlayerInputManager>();
           inputManager.choice = (GameChoices)i;
           choice.GetComponent<Button>().onClick.AddListener(inputManager.GetChoice);
       }
   }
   public void DestoryAllChildren()
   {
       for(int i = 0;i<choiceParent.childCount;i++)
       {
           Destroy(choiceParent.GetChild(i).gameObject);
       }
   }


   IEnumerator RestartGame()
   {

       yield return new WaitForSeconds(1f);
       
   }
}
