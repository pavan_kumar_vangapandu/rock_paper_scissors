using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;
using UnityEngine.Events;

public class UIManager : MonoBehaviour
{
    #region UI
    [SerializeField]
    private Text infoText;

    [SerializeField]
    private Text oppenentChoiceText;
    [SerializeField]
    private GameObject mainMenuPanel;
    [SerializeField]
    private GameObject gamePlayPanel;
    [SerializeField]
    private Text highScoretext;
    #endregion

    #region Events
    public UnityEvent<string> updateInfoText;

    public UnityEvent<string> updateoppenentChoiceText;

    public UnityEvent updategameState;
    public UnityEvent StartGame;

    #endregion

    
    #region references
    GamePlayController controller;
    TimeManager timeManager;
    #endregion
    private void Start() {
         controller = GameObject.FindObjectOfType<GamePlayController>();
         
        updateInfoText.AddListener(ShowWinLoseText);
        updateoppenentChoiceText.AddListener(ShowOpponentChoice);
        updategameState.AddListener(UpdateGameStateUI);
        StartGame.AddListener(PlayButton);
        controller.updateoppenentChoiceText = updateoppenentChoiceText;
        controller.updateInfoText = updateInfoText;
        controller.updategameState = updategameState;
        controller.startGame = StartGame;
        ShowHighScore();

        //Debug.Log("Show Listeners :"+controller.updateoppenentChoiceText.GetPersistentEventCount);

    }
    public void ShowWinLoseText(string message)
    {
        infoText.text = message;
    }

    public void ShowOpponentChoice(string message)
    {
        oppenentChoiceText.text = message;
    }
    public void UpdateGameStateUI()
    {
       switch(controller.gameStates) 
       {
           case GameStates.MainMenu:
                mainMenuPanel.SetActive(true);
                gamePlayPanel.SetActive(false);
                ScoreManager.currentscore = 0;
                ShowHighScore();
                break;
            case GameStates.GamePlay:
                mainMenuPanel.SetActive(false);
                gamePlayPanel.SetActive(true);
                break;
       }
    }

    public void PlayButton()
    {
        controller.playerChoice = GameChoices.NONE;
        controller.opponentChoice = GameChoices.NONE;
        timeManager = GameObject.FindObjectOfType<TimeManager>();
        if(timeManager)
            timeManager.ResetTimer();
        controller.gameStates = GameStates.GamePlay;
        GetComponent<AiInputManager>().StartGame();
        infoText.text = "";
        updategameState.Invoke();
    }

    public void ShowHighScore()
    {
        highScoretext.text = ScoreManager.HightScore.ToString();
    }
}
