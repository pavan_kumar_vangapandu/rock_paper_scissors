using System.Collections;
using System.Collections.Generic;
using System.Linq;
using UnityEngine;
using UnityEngine.Events;
using UnityEngine.UI;
public enum GameStates {
    MainMenu,
    GamePlay
}
public enum GameChoices {
    NONE = 0,
    Rock = 1,
    Paper = 2,
    Scissors = 3,
    Lizard = 4,
    Spock = 5
}
public class GamePlayController : MonoBehaviour {
    public static List<Rule> Rules = new List<Rule> {
        new Rule (GameChoices.Scissors, "cuts", GameChoices.Paper),
        new Rule (GameChoices.Paper, "covers", GameChoices.Rock),
        new Rule (GameChoices.Rock, "crushes", GameChoices.Lizard),
        new Rule (GameChoices.Lizard, "poisons", GameChoices.Spock),
        new Rule (GameChoices.Spock, "smashes", GameChoices.Scissors),
        new Rule (GameChoices.Scissors, "decapitates", GameChoices.Lizard),
        new Rule (GameChoices.Lizard, "eats", GameChoices.Paper),
        new Rule (GameChoices.Paper, "disproves", GameChoices.Spock),
        new Rule (GameChoices.Spock, "vaporizes", GameChoices.Rock),
        new Rule (GameChoices.Rock, "crushes", GameChoices.Scissors),
    };
    public GameChoices playerChoice = GameChoices.NONE;
    public GameChoices opponentChoice = GameChoices.NONE;

    public UnityEvent<string> updateInfoText;

    public UnityEvent<string> updateoppenentChoiceText;
    public UnityEvent updategameState;
    public UnityEvent startGame;
    public UnityEvent UpdateScore; //event assigned in editor

    public GameStates gameStates = GameStates.MainMenu;
    public void SetPlayerChoice (GameChoices gameChoice) {

        playerChoice = gameChoice;

        DecideWinner ();

    }

    public void SetOpponentChoice (GameChoices gameChoice) {

        int rnd = Random.Range (1, 6);
        opponentChoice = (GameChoices) rnd;
        StartCoroutine (UpdateOpponentText ());
    }
    IEnumerator UpdateOpponentText () {
        yield return new WaitForEndOfFrame ();
        updateoppenentChoiceText.Invoke (opponentChoice.ToString ().ToUpper ());
    }
    void DecideWinner () {

        string message = "";
        var playerRule = FindRulePlayer();
        var oppRule = FindRuleOpponent();
        if(playerRule != null)
        {
            gameStates = GameStates.GamePlay;
            message = "You Win!";
            UpdateScore.Invoke();
            startGame.Invoke();
        }
        else if(oppRule != null)
        {
            gameStates = GameStates.MainMenu;
            message = "You Lose!";
        }
        else
        {
            gameStates = GameStates.GamePlay;
            message = "It's a Draw!";
            startGame.Invoke();
        }
        Debug.Log(message);
        updateInfoText.Invoke (message.ToUpper());
        updategameState.Invoke();
    }
    public Rule FindRulePlayer () {
        return Rules.FirstOrDefault (rule => rule.roundWinner == playerChoice && rule.roundLoser == opponentChoice);
    }

    public Rule FindRuleOpponent () {
        return Rules.FirstOrDefault (rule => rule.roundWinner == opponentChoice && rule.roundLoser == playerChoice);
    }

    
}