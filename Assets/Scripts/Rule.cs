using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Rule
    {
        public GameChoices roundWinner;
        public GameChoices roundLoser;
        public string verb;

        public Rule(GameChoices roundWinner_input, string verb_input, GameChoices roundLoser_input)
        {
            roundWinner = roundWinner_input;
            verb = verb_input;
            roundLoser = roundLoser_input;
        }
    }
