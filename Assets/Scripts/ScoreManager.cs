using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.Events;

public class ScoreManager : MonoBehaviour
{
    public ScoringSO data;
    public static int HightScore
    {
        get
        {
            return PlayerPrefs.GetInt("high_score",0);
        }
        set
        {
            PlayerPrefs.SetInt("high_score",value);
            PlayerPrefs.Save();
        }
    }
    public static int currentscore;
    int scoreToincrease;
    private void Start() {
        scoreToincrease = data.scoringPoints;
    }
    public void SetHighScore(int score)
    {
        if(score > HightScore)
        {
            HightScore = score;
        }
    }
    public void UpdateScore()
    {
        currentscore += scoreToincrease;
        SetHighScore(currentscore);
    }
}
