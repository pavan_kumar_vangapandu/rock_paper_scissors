using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class InputManager : MonoBehaviour
{
    [HideInInspector]
    public GamePlayController controller;
    public GameChoices choice;
    
    public virtual void GetChoice()
    {

    }
}
