using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AiInputManager : InputManager
{
   private void Awake() {
       controller = GameObject.FindObjectOfType<GamePlayController>();
   }
    public void StartGame()
    {
        controller.gameStates = GameStates.GamePlay;
        GetChoice();
    }

    public override void GetChoice()
    {
        SetOpponentChoice();
    }
    void SetOpponentChoice () {

        int rnd = Random.Range (1, 6);
        controller.SetOpponentChoice((GameChoices) rnd);
    }

}
