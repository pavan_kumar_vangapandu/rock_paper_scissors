using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlayerInputManager : InputManager
{
    private void Awake() {
        controller = GameObject.FindObjectOfType<GamePlayController>();
    }
    public override void GetChoice()
    {
        controller.SetPlayerChoice(choice);
    }
    
}
