Timer :
- we can edit timer value in /Assets/SO/Data.asset file
- Timer Logic Handled in /Assets/Scripts/TimeManager.cs Script

Score :
- we can edit score value in /Assets/SO/Data.asset file
- Scoring Logic handled in /Assets/Scripts/ScoreManager.cs script

Game Logic:
- all the game logic (game rules and deciding winner) handled in /Assets/Scripts/GamePlayController.cs script
- Updating UI and Score is handled through unity events to make the GameplayController script independent from other scripts 

UI:
- completely used unity default images.
- used unity events to update the UI texts

input logic:
- scripts are available in /Assets/Scripts/Input folder
- used inheritance concept to separate AI input and player input.

Player Choices :
- Dynamic spawning of player choice buttons logic is handled in /Assets/Scripts/PlayerchoiceSpawner.cs script.

code from forums :

- I found some code that is interesting in this thread 
   https://codereview.stackexchange.com/questions/244745/rock-paper-scissors-lizard-spock-in-c

- I used the small part of code where the winning rules for AI and player predefined in list of Rule calss. I used it because I don't want
    to use if else conditions here to decide win or lose condition.
    public static List<Rule> Rules = new List<Rule>
    {
            new Rule(Choice.Scissors, "cuts", Choice.Paper),
            new Rule(Choice.Paper, "covers", Choice.Rock),
            new Rule(Choice.Rock, "crushes", Choice.Lizard),
            new Rule(Choice.Lizard, "poisons", Choice.Spock),
            new Rule(Choice.Spock, "smashes", Choice.Scissors),
            new Rule(Choice.Scissors, "decapitates", Choice.Lizard),
            new Rule(Choice.Lizard, "eats", Choice.Paper),
            new Rule(Choice.Paper, "disproves", Choice.Spock),          
            new Rule(Choice.Spock, "vaporizes", Choice.Rock),
            new Rule(Choice.Rock, "crushes", Choice.Scissors),
    };
        
- the above code used in /Assets/Scripts/GamePlayController.cs script

- Rest of the code written from scratch
        



